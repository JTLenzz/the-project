# Explicitly specify GCC cross compiler.
CC := arm-none-eabi-gcc
CXX := arm-none-eabi-g++
AR := arm-none-eabi-ar

BUILD_DIR := bin
LIB_DIR := lib

WARNING_FLAGS := -Wall -Wextra
ARCH_FLAGS := -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant
COMPILE_FLAGS := -Og -ggdb -fomit-frame-pointer -ffunction-sections -fdata-sections -fno-common -mno-thumb-interwork
DEFINES := -DSTM32F407xx -DUSE_HAL_DRIVER
CFLAGS := ${ARCH_FLAGS} ${COMPILE_FLAGS} ${WARNING_FLAGS} ${DEFINES} -std=c99
CXXFLAGS := ${ARCH_FLAGS} ${COMPILE_FLAGS} ${WARNING_FLAGS} ${DEFINES} -std=c++11 -fno-rtti -fno-exceptions
ASFLAGS := ${ARCH_FLAGS} ${COMPILE_FLAGS} ${DEFINES} -x assembler-with-cpp
LDFLAGS := ${ARCH_FLAGS} ${COMPILE_FLAGS} -Wl,--gc-sections

SUBMAKEFILES := \
	project.mk \
	ThirdPartySupport/cmsis.mk \
	ThirdPartySupport/HalDriver.mk


