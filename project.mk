TARGET := project.elf

MAP_FILE := ${BUILD_DIR}/$(patsubst %.elf,%.map,${TARGET})

TGT_LDLIBS := -lcmsis -lhaldriver
TGT_PREREQS := ${LIB_DIR}/libcmsis.a ${LIB_DIR}/libhaldriver.a
TGT_LDFLAGS :=-Wl,-TThirdParty/CMSIS/Device/ST/STM32F4xx/STM32F407ZGTx_FLASH.ld \
	-Wl,-Map=${MAP_FILE},--cref \
	-L${LIB_DIR}

SOURCES := \
	interrupts.cpp \
	main.cpp

SRC_INCDIRS := \
	ThirdParty/CMSIS/Device/ST/STM32F4xx/Include \
	ThirdParty/CMSIS/Include \
	ThirdParty/STM32F4xx_HAL_Driver/Inc \
	ThirdPartySupport/STM32F4xx_HAL_Driver
