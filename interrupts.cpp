/**
 * @file interrupts.cpp
 * @brief Interrupt handler routines.
 * @copyright Original code by Jonathan Lenz
 *  This software is provided 'as-is', without any express or implied
 *  warranty. In no event will the authors be held liable for any
 *  damages arising from the use of this software.
 */

#include "stm32f4xx_hal.h"

/**
 * @brief This function handles Non Maskable Interrupt.
 */
extern "C" void NMI_Handler() {
  HAL_RCC_NMI_IRQHandler();
}

/**
 * @brief Handler for the system tick interrupt source.
 */
extern "C" void SysTick_Handler() {
  HAL_IncTick();
}
