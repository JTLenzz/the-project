TARGET := ${LIB_DIR}/libhaldriver.a

HAL_PATH := ../ThirdParty/STM32F4xx_HAL_Driver
CMSIS_PATH := ../ThirdParty/CMSIS

SOURCES := \
	${HAL_PATH}/Src/stm32f4xx_hal.c \
	${HAL_PATH}/Src/stm32f4xx_hal_adc.c \
	${HAL_PATH}/Src/stm32f4xx_hal_adc_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_can.c \
	${HAL_PATH}/Src/stm32f4xx_hal_cec.c \
	${HAL_PATH}/Src/stm32f4xx_hal_cortex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_crc.c \
	${HAL_PATH}/Src/stm32f4xx_hal_cryp.c \
	${HAL_PATH}/Src/stm32f4xx_hal_cryp_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_dac.c \
	${HAL_PATH}/Src/stm32f4xx_hal_dac_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_dcmi.c \
	${HAL_PATH}/Src/stm32f4xx_hal_dcmi_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_dma.c \
	${HAL_PATH}/Src/stm32f4xx_hal_dma2d.c \
	${HAL_PATH}/Src/stm32f4xx_hal_dma_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_eth.c \
	${HAL_PATH}/Src/stm32f4xx_hal_flash.c \
	${HAL_PATH}/Src/stm32f4xx_hal_flash_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_flash_ramfunc.c \
	${HAL_PATH}/Src/stm32f4xx_hal_fmpi2c.c \
	${HAL_PATH}/Src/stm32f4xx_hal_fmpi2c_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_gpio.c \
	${HAL_PATH}/Src/stm32f4xx_hal_hash.c \
	${HAL_PATH}/Src/stm32f4xx_hal_hash_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_hcd.c \
	${HAL_PATH}/Src/stm32f4xx_hal_i2c.c \
	${HAL_PATH}/Src/stm32f4xx_hal_i2c_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_i2s.c \
	${HAL_PATH}/Src/stm32f4xx_hal_i2s_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_irda.c \
	${HAL_PATH}/Src/stm32f4xx_hal_iwdg.c \
	${HAL_PATH}/Src/stm32f4xx_hal_ltdc.c \
	${HAL_PATH}/Src/stm32f4xx_hal_msp_template.c \
	${HAL_PATH}/Src/stm32f4xx_hal_nand.c \
	${HAL_PATH}/Src/stm32f4xx_hal_nor.c \
	${HAL_PATH}/Src/stm32f4xx_hal_pccard.c \
	${HAL_PATH}/Src/stm32f4xx_hal_pcd.c \
	${HAL_PATH}/Src/stm32f4xx_hal_pcd_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_pwr.c \
	${HAL_PATH}/Src/stm32f4xx_hal_pwr_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_qspi.c \
	${HAL_PATH}/Src/stm32f4xx_hal_rcc.c \
	${HAL_PATH}/Src/stm32f4xx_hal_rcc_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_rng.c \
	${HAL_PATH}/Src/stm32f4xx_hal_rtc.c \
	${HAL_PATH}/Src/stm32f4xx_hal_rtc_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_sai.c \
	${HAL_PATH}/Src/stm32f4xx_hal_sai_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_sd.c \
	${HAL_PATH}/Src/stm32f4xx_hal_sdram.c \
	${HAL_PATH}/Src/stm32f4xx_hal_smartcard.c \
	${HAL_PATH}/Src/stm32f4xx_hal_spdifrx.c \
	${HAL_PATH}/Src/stm32f4xx_hal_spi.c \
	${HAL_PATH}/Src/stm32f4xx_hal_sram.c \
	${HAL_PATH}/Src/stm32f4xx_hal_tim.c \
	${HAL_PATH}/Src/stm32f4xx_hal_tim_ex.c \
	${HAL_PATH}/Src/stm32f4xx_hal_uart.c \
	${HAL_PATH}/Src/stm32f4xx_hal_usart.c \
	${HAL_PATH}/Src/stm32f4xx_hal_wwdg.c \
	${HAL_PATH}/Src/stm32f4xx_ll_fmc.c \
	${HAL_PATH}/Src/stm32f4xx_ll_fsmc.c \
	${HAL_PATH}/Src/stm32f4xx_ll_sdmmc.c \
	${HAL_PATH}/Src/stm32f4xx_ll_usb.c

SRC_INCDIRS := \
	${CMSIS_PATH}/Device/ST/STM32F4xx/Include \
	${CMSIS_PATH}/Include \
	${HAL_PATH}/Inc \
	STM32F4xx_HAL_Driver
