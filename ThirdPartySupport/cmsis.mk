TARGET := ${LIB_DIR}/libcmsis.a

CMSIS_PATH = ../ThirdParty/CMSIS
HAL_PATH = ../ThirdParty/STM32F4xx_HAL_Driver

SOURCES := \
	${CMSIS_PATH}/Device/ST/STM32F4xx/Source/Templates/system_stm32f4xx.c \
	${CMSIS_PATH}/Device/ST/STM32F4xx/Source/Templates/gcc/startup_stm32f407xx.s

SRC_INCDIRS := \
	${CMSIS_PATH}/Device/ST/STM32F4xx/Include \
	${CMSIS_PATH}/Include \
	${HAL_PATH}/Inc \
	STM32F4xx_HAL_Driver
