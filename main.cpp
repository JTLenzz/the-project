/**
 * @file main.cpp
 * @brief Main user program entry point.
 * @copyright Original code by Jonathan Lenz
 *  This software is provided 'as-is', without any express or implied
 *  warranty. In no event will the authors be held liable for any
 *  damages arising from the use of this software.
 */

#include "stm32f4xx_hal.h"

/**
 * @brief Main device software entry point.
 * @return Should never return.
 */
extern "C" int main() {
  HAL_Init();

  for (;;) {
    __WFI();
  }
}
